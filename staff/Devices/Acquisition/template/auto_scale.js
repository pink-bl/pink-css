importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var Scalemin = PVUtil.getDouble(pvs[0]);
var Scalemax = PVUtil.getDouble(pvs[1]);
var Statmaxval = PVUtil.getDouble(pvs[2]);
var Statminval = PVUtil.getDouble(pvs[3]);
var Autoscale = PVUtil.getDouble(pvs[4]);

if(Autoscale==1){
	widget.setPropertyValue("minimum", Statminval);
	widget.setPropertyValue("maximum", Statmaxval);
}else{
	widget.setPropertyValue("minimum", Scalemin);
	widget.setPropertyValue("maximum", Scalemax);	
}