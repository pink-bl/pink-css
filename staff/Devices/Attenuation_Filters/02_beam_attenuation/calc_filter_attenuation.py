#!/usr/bin/python3

#Created on Tue Jun 19 16:46:27 2018 @author: julius bauer, MPI CEC, PINK project

#-----------------------------------------------------------------------------
# COMMENT: 
#
# If you want to use 4th filter holder, uncomment respective code lines
# If you want to use >3 materials as filters (up to 5), you need to change...
#   ...in db defining PVs: 
#       - nothing!
#   ...in this script:
#       - append (but do NOT shuffle order) of 'elementlist'
#       - add 'densityX' at line 248
#       - add 'crossecX' at line 253
#       - add 'tX' calculation below line 267
#       - add to calculation of 'absorptionvalue' factor(s) 'tX' in line 270
#   ...in the database holding values for density and filter thicknesses:
#       - append 'Density' list by additional densiti(es)
#       - append list elements holding info on filter thicknesses for ALL holder 
#       
#-----------------------------------------------------------------------------
#"""

import numpy
import time
import sys
from epics import PV
# from org.csstudio.opibuilder.scriptUtil import ConsoleUtil
# ConsoleUtil.writeInfo("...start...")
from xraydb import XrayDB

# --------------------- definitions and more ---------------------------------

elementlist         = ['C','AL','TA']
numofelements       = len(elementlist)
mbboSTRfields       = ['ZRST', 'ONST', 'TWST', 'THST', 'FRST', 'FVST', 'SXST', 'SVST', 'EIST', 'NIST', 'TEST', 'ELST', 'TVST', 'TTST', 'FTST', 'FFST']
mbboVALfields       = ['ZRVL', 'ONVL', 'TWVL', 'THVL', 'FRVL', 'FVVL', 'SXVL', 'SVVL', 'EIVL', 'NIVL', 'TEVL', 'ELVL', 'TVVL', 'TTVL', 'FTVL', 'FFVL']

PVpath              = 'PINK:VM10'

OBJ_InfoMessage     = PV(PVpath + ':message') 
# holds up to 40 characters
OBJ_InfoMessage.put('milestone 00')

obj_PhotonEnergy    = PV(PVpath + ':photonenergy.VAL')
obj_attenuations    = PV(PVpath + ':attenuationvalues')
    
obj_descA           = PV(PVpath + ':filtersetA.DESC')
obj_descB           = PV(PVpath + ':filtersetB.DESC')
obj_descC           = PV(PVpath + ':filtersetC.DESC')
#obj_descD           = PV(PVpath + ':filtersetD.DESC')
obj_filterA         = PV(PVpath + ':filtersetA')
obj_filterB         = PV(PVpath + ':filtersetB')
obj_filterC         = PV(PVpath + ':filtersetC')
#obj_filterD         = PV(PVpath + ':filtersetD')
obj_infofilterA     = PV(PVpath + ':mbbofilterA')
obj_infofilterB     = PV(PVpath + ':mbbofilterB')
obj_infofilterC     = PV(PVpath + ':mbbofilterC')
#obj_infofilterD     = PV(PVpath + ':mbbofilterD')


firsttimeA          = obj_descA.get()
firsttimeB          = obj_descB.get()
firsttimeC          = obj_descC.get()
#firsttimeD          = obj_descD.get()
photonenergy        = obj_PhotonEnergy.get()

um_to_cm            = 1/10000      
# conversion factor to converts thicknesses in um as in imported array to cm as needed for calculation

OBJ_InfoMessage.put('milestone 01')


# -----------------------------------------------------------------------------
# --------------------- will run if PVs are empty after restart of IOC --------
# -----------------------------------------------------------------------------

# ................... fill PV for filter holder A .............................
# .............................................................................
if (firsttimeA == "empty"):
    # if PV for A is new it is here filled with density and thickness values 
    from database_script_calc_filter_attenuation import Density
    from database_script_calc_filter_attenuation import FilterThicknessesA

    numoffilters = len(FilterThicknessesA)
    materiallist = []
    
    # create list that holds values for density and filter thicknesses
    for k in range(0,numofelements):
        while len(materiallist) < k*100:
            materiallist.append(0)
        materiallist.append(Density[k])
        for i in range (0,numoffilters):
            materiallist.append(FilterThicknessesA[i][k+1])
    
    # write list to PV for later uses
    obj_filterA.put(materiallist)
    
    # transfer filter information and fill in numbers for filter position
    for j in range(0,numoffilters):
        PV(PVpath + ':mbbofilterA.' + mbboVALfields[j]).put(j)
        PV(PVpath + ':mbbofilterA.' + mbboSTRfields[j]).put(FilterThicknessesA[j][0])
    
    # add info on number of filter in this holder
    obj_descA.put(str(numoffilters))
    
# ................... fill PV for filter holder B .............................
# .............................................................................
if (firsttimeB == "empty"):
    OBJ_InfoMessage.put('milestone 02a')
    # if PV for B is new it is here filled with density and thickness values 
    from database_script_calc_filter_attenuation import Density
    from database_script_calc_filter_attenuation import FilterThicknessesB

    numoffilters = len(FilterThicknessesB)
    materiallist = []
    
    # create list that holds values for density and filter thicknesses
    for k in range(0,numofelements):
        while len(materiallist) < k*100:
            materiallist.append(0)
        materiallist.append(Density[k])
        for i in range (0,numoffilters):
            materiallist.append(FilterThicknessesB[i][k+1])
    
    # write list to PV for later uses
    obj_filterB.put(materiallist)
    
    # transfer filter information and fill in numbers for filter position
    for j in range(0,numoffilters):
        PV(PVpath + ':mbbofilterB.' + mbboVALfields[j]).put(j)
        PV(PVpath + ':mbbofilterB.' + mbboSTRfields[j]).put(FilterThicknessesB[j][0])
    
    # add info on number of filter in this holder
    obj_descB.put(str(numoffilters))

OBJ_InfoMessage.put('milestone 02b')

# ................... fill PV for filter holder C .............................
# .............................................................................
if (firsttimeC == "empty"):
    OBJ_InfoMessage.put('milestone 03a')
    # if PV for C is new it is here filled with density and thickness values 
    from database_script_calc_filter_attenuation import Density
    from database_script_calc_filter_attenuation import FilterThicknessesC

    numoffilters = len(FilterThicknessesC)
    materiallist = []
    
    # create list that holds values for density and filter thicknesses
    for k in range(0,numofelements):
        while len(materiallist) < k*100:
            materiallist.append(0)
        materiallist.append(Density[k])
        for i in range (0,numoffilters):
            materiallist.append(FilterThicknessesC[i][k+1])
    
    # write list to PV for later uses
    obj_filterC.put(materiallist)
    
    # transfer filter information and fill in numbers for filter position
    for j in range(0,numoffilters):
        PV(PVpath + ':mbbofilterC.' + mbboVALfields[j]).put(j)
        PV(PVpath + ':mbbofilterC.' + mbboSTRfields[j]).put(FilterThicknessesC[j][0])
    
    # add info on number of filter in this holder
    obj_descC.put(str(numoffilters))

OBJ_InfoMessage.put('milestone 03b')

# ................... fill PV for filter holder D .............................
# .............................................................................
#if (firsttimeD == "empty"):
#    # if PV for D is new it is here filled with density and thickness values 
#    from database_script_calc_filter_attenuation import Density
#    from database_script_calc_filter_attenuation import FilterThicknessesD
#
#    numoffilters = len(FilterThicknessesD)
#    materiallist = []
#    
#    # create list that holds values for density and filter thicknesses
#    for k in range(0,numofelements):
#        while len(materiallist) < k*100:
#            materiallist.append(0)
#        materiallist.append(Density[k])
#        for i in range (0,numoffilters):
#            materiallist.append(FilterThicknessesD[i][k+1])
#    
#    # write list to PV for later uses
#    obj_filterD.put(materiallist)
#    
#    # transfer filter information and fill in numbers for filter position
#    for j in range(0,numoffilters):
#        PV(PVpath + ':mbbofilterD.' + mbboVALfields[j]).put(j)
#        PV(PVpath + ':mbbofilterD.' + mbboSTRfields[j]).put(FilterThicknessesD[j][0])
#    
#    # add info on number of filter in this holder
#    obj_descD.put(str(numoffilters))

# -----------------------------------------------------------------------------
# --------------------- functions ---------------------------------------------
# -----------------------------------------------------------------------------

def CalcTransmission(crossSection, density, thickness):
    # calculates transmission
    # NOTE: thickness in um !!, density in g/cm3, crossSection in cm2/g
    try: 
        exponent = -1.0 * crossSection 
    except:
        OBJ_InfoMessage.put('Problem with cross section')
        time.sleep(2)
        sys.exit()
    try:
        exponent = exponent * density
    except:
        OBJ_InfoMessage.put('Problem with density')
        time.sleep(2)
        sys.exit()   
    try:
        exponent = exponent * um_to_cm * thickness
    except:
        OBJ_InfoMessage.put('Problem with density')
        time.sleep(2)
        sys.exit()       
    transmission = numpy.exp(exponent)
    return transmission


def GetCrossSection(element):
    # reads cross section from Xray database
    # NOTE: uses earlier defined 'photonenergy'
    crossSection = XrayDB().cross_section_elam(element, photonenergy, kind='photo')
    print(element + ' cross section: ' + str(crossSection) + '[cm^2/g]')
    return crossSection
    
#    try:
#        crossSection = XrayDB().cross_section_elam(element, photonenergy, kind='photo')
#        print(element + ' cross section: ' + str(crossSection) + '[cm^2/g]')
#        return crossSection
#    except:
#        print('Problem to get cross section for entry:' + element)
#        OBJ_InfoMessage.put('Problem: cross section for ' + element)
#        time.sleep(2)
#        sys.exit()




# -----------------------------------------------------------------------------
# --------------------- main --------------------------------------------------
# -----------------------------------------------------------------------------


filtersetA          = obj_filterA.get()
# print(filtersetA)
filtersetB          = obj_filterB.get()
filtersetC          = obj_filterC.get()
#filtersetD          = obj_filterD.get()
# reads PV holding info on material density and thickness of filters in each holder



numoffilterA        = int(obj_descA.get())
# print(numoffilterA)
numoffilterB        = int(obj_descB.get())
numoffilterC        = int(obj_descC.get())
#numoffilterD        = int(obj_descD.get())
# reads in value describing number of filters in each holder

density1            = filtersetA[0]
# print(density1)
density2            = filtersetA[100]
density3            = filtersetA[200]
# takes info on material density from filter infos on holder A

# print('densities: ' + str(density1) + ', '+ str(density2) + ', '+ str(density3) + '; all in [gm/cm^3]')

crossec1 = GetCrossSection(elementlist[0])
crossec2 = GetCrossSection(elementlist[1])
crossec3 = GetCrossSection(elementlist[2])
# calculates cross sections for each filter material

absorptionlist = []
# creates list that later is sent to PV holding all absorption values of the filters


# ................... list entries for filter holder A ........................
# .............................................................................
numoffilter = numoffilterA
filterset = filtersetA
for i in range(1, numoffilter +1):
    t1 = CalcTransmission(crossec1, density1, filterset[(i +0)])
    t2 = CalcTransmission(crossec2, density2, filterset[(i +100)])
    t3 = CalcTransmission(crossec3, density3, filterset[(i +200)])
    absorptionvalue = 1 - (t1*t2*t3)
    absorptionlist.append(absorptionvalue)

while len(absorptionlist) < 100:
    absorptionlist.append(0)
# filling list with zeros until 100 elements to allow values for filter B
# to begin at position list[100] as list is zero-based
        
# ................... list entries for filter holder B ........................
# .............................................................................
numoffilter = numoffilterB
filterset = filtersetB
for i in range(1, numoffilter +1):
    t1 = CalcTransmission(crossec1, density1, filterset[(i +0)])
    t2 = CalcTransmission(crossec2, density2, filterset[(i +100)])
    t3 = CalcTransmission(crossec3, density3, filterset[(i +200)])
    absorptionvalue = 1 - (t1*t2*t3)
    absorptionlist.append(absorptionvalue)
    
while len(absorptionlist) < 200:
    absorptionlist.append(0)

# ................... list entries for filter holder C ........................
# .............................................................................
numoffilter = numoffilterC
filterset = filtersetC
for i in range(1, numoffilter +1):
    t1 = CalcTransmission(crossec1, density1, filterset[(i +0)])
    t2 = CalcTransmission(crossec2, density2, filterset[(i +100)])
    t3 = CalcTransmission(crossec3, density3, filterset[(i +200)])
    absorptionvalue = 1 - (t1*t2*t3)
    absorptionlist.append(absorptionvalue)

while len(absorptionlist) < 300:
    absorptionlist.append(0)
    
## ................... list entries for filter holder D .......................
## ............................................................................
#numoffilter = numoffilterD
#filterset = filtersetD
#for i in range(1, numoffilter +1):
#    t1 = CalcTransmission(crossec1, density1, filterset[(i +0)])
#    t2 = CalcTransmission(crossec2, density2, filterset[(i +100)])
#    t3 = CalcTransmission(crossec3, density3, filterset[(i +200)])
#    absorptionvalue = 1 - (t1*t2*t3)
#    absorptionlist.append(absorptionvalue)

while len(absorptionlist) < 320:
    absorptionlist.append(0)
# filling list with zeros until 320 elements to fill waveform holding absorption
# values and overwrites older entries in that waveform

#..............................................................................

obj_attenuations.put(absorptionlist)
# writes list to waveform holding absorption values

OBJ_InfoMessage.put('Finished')
# send message to indicate end of script
print('FINISH')
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~The End of Code ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
