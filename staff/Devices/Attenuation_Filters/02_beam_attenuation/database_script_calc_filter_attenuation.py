# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 13:48:27 2018

@author: julius
"""
# Densities in [gm/cm^3], sources: diamond <- wikipedia, AL & Ta <- ELAM database
# 'diamond, Al, Ta':
Density = [3.52 , 2.694, 16.624]

FieldsMbbo_str = ['ZRST', 'ONST', 'TWST', 'THST', 'FRST', 'FVST', 'SXST', 'SVST', 'EIST', 'NIST', 'TEST', 'ELST', 'TVST', 'TTST', 'FTST', 'FFST']

# [0] = description, [1] = C_diamond [um], [2] = Al [um], [3] = Ta [um]
FilterThicknessesA = [
['A00: holes', 0,0,0], 
['A01: 50 0 0', 50,0,0],
['A02: 100 0 0', 100,0,0],
['A02: 150 0 0', 150,0,0],
['A03: 200 0 0', 200,0,0],
['A04: 0 40 0', 0,40,0],
['A05: 0 0 1', 0,0,1],
['A06: 0 0 2', 0,0,2],
['A07: 0 0 4', 0,0,4],
['A09: 100 40 4', 100,40,4],
['A10: 110 44 4.4', 110, 44, 4.4]]
# Filter A (first hit by beam)

FilterThicknessesB = [
['Ab00: holes', 0,0,0], 
['Ab01: 50 0 0', 50,0,0],
['Ab02: 100 0 0', 100,0,0],
['Ab02: 150 0 0', 150,0,0],
['Ab03: 200 0 0', 200,0,0],
['Ab04: 0 40 0', 0,40,0],
['Ab05: 0 0 1', 0,0,1],
['Ab06: 0 0 2', 0,0,2],
['Ab07: 0 0 4', 0,0,4],
['Ab09: 100 40 4', 100,40,4],
['Ab10: 110 44 4.4', 110, 44, 4.4]]
# Filter A (first hit by beam)

FilterThicknessesC = [
['Ac00: holes', 0,0,0], 
['Ac01: 50 0 0', 50,0,0],
['Ac02: 100 0 0', 100,0,0],
['Ac02: 150 0 0', 150,0,0],
['Ac03: 200 0 0', 200,0,0],
['Ac04: 0 40 0', 0,40,0],
['Ac05: 0 0 1', 0,0,1],
['Ac06: 0 0 2', 0,0,2],
['Ac07: 0 0 4', 0,0,4],
['Ac09: 100 40 4', 100,40,4],
['Ac10: 110 44 4.4', 110, 44, 4.4]]
# Filter A (first hit by beam)

FilterThicknessesD = [
['Ad00: holes', 0,0,0], 
['Ad01: 50 0 0', 50,0,0],
['Ad02: 100 0 0', 100,0,0],
['Ad02: 150 0 0', 150,0,0],
['Ad03: 200 0 0', 200,0,0],
['Ad04: 0 40 0', 0,40,0],
['Ad05: 0 0 1', 0,0,1],
['Ad06: 0 0 2', 0,0,2],
['Ad07: 0 0 4', 0,0,4],
['Ad09: 100 40 4', 100,40,4],
['Ad10: 110 44 4.4', 110, 44, 4.4]]
# Filter A (first hit by beam)