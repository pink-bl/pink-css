from org.csstudio.opibuilder.scriptUtil import PVUtil
from org.csstudio.opibuilder.scriptUtil import ColorFontUtil
from org.csstudio.opibuilder.scriptUtil import ConsoleUtil

ConsoleUtil.writeInfo("starting")

table = widget.getTable()
NumOfFilters = 11

ConsoleUtil.writeInfo("marker 01")

#Fill PV Name only once
if widget.getVar("firstTime") == None:
    widget.setVar("firstTime", True)
    i=0
    ConsoleUtil.writeInfo("...1st run...")
    #for pv in pvs:
    
    # start with 1 and run to NumOfFilters plus 2
    for x in range(2, NumOfFilters +2):
        table.setCellText(i, 0, PVUtil.getString(pvs[x]))
        if not pvs[x].isConnected():
            table.setCellText(i, 1, "Disconnected")
        i+=1

ConsoleUtil.writeInfo("marker 02")
##find index of the trigger PV
#i=0
#while triggerPV != pvs[i]:
#    i+=1
#
#table.setCellText(i, 1, PVUtil.getString(triggerPV))
#table.setCellText(i, 2, PVUtil.getTimeString(triggerPV))
## hier noch die pv umstellen auf wave form und mbbos und display inhalte anpassen

if triggerPV == pvs[0]:
    ConsoleUtil.writeInfo("marker 05")
    Waveform = PVUtil.getDoubleArray(pvs[0])    
    ConsoleUtil.writeInfo(str(Waveform[0]))
    ConsoleUtil.writeInfo("marker 06")
    for k in range(0, NumOfFilters):
        table.setCellText(k, 1, str(Waveform[k]))
    ConsoleUtil.writeInfo("marker 07")




if triggerPV == pvs[1]:
    ConsoleUtil.writeInfo("marker 10")
    s       = int(PVUtil.getLong(pvs[1]))
    white   = ColorFontUtil.WHITE
    green   = ColorFontUtil.GREEN
    gray    = ColorFontUtil.getColorFromRGB(240, 240, 240)
    ConsoleUtil.writeInfo("marker 11")
    for j in range(0, NumOfFilters):
        if s == j:
            table.setCellBackground(j, 1, green)
        else:
            if (2*int(j/2) == j):
                table.setCellBackground(j, 1, white)
            else:
                table.setCellBackground(j, 1, gray)
            




ConsoleUtil.writeInfo("finished!")
ConsoleUtil.writeInfo("")
ConsoleUtil.writeInfo("")