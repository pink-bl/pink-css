from org.csstudio.opibuilder.scriptUtil import PVUtil
from org.csstudio.opibuilder.scriptUtil import ConsoleUtil
import array

## Variables
energy = PVUtil.getDouble(pvs[0])
transmission = pvs[1]
fa_str = pvs[2]
fa_val = pvs[3]
test = pvs[4]

#test.setValue

## Console printing example
#ConsoleUtil.writeInfo("Energy changed")

## Populate table strings
filter_list_A = ["Carbon 10um","Carbon 20um","Magic inf"]
fa_str.setValue(filter_list_A)

## Calculate table values
filter_A = array.array('d',([0]*3))
filter_A[0]=(energy/1000.0)*1
filter_A[1]=(energy/1000)*2.0
filter_A[2]=(energy/1000.0)*3.0

fa_val.setValue(filter_A)



pvs[1].setValue(energy)
#pvs[4].setValue(test)






