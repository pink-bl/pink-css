#!/usr/bin/python3
import time
import epics
import sys

motor1 = epics.PV("PINK:MSIM:m1.VAL", auto_monitor='TRUE')
LED = epics.PV("PINK:VM10:bo", auto_monitor='FALSE')
status = epics.PV("PINK:VM10:stringout", auto_monitor='FALSE')
script_status = epics.PV("PINK:VM10:ao", auto_monitor='TRUE')

if (script_status.value):
    sys.exit()

script_status.put(1)

for i in range(6):
    next_pos=3*i
    
#    print("Moving to: ", next_pos)
#    sys.stdout.flush()

    str = "Moving to %.2f" % (next_pos)
    status.put(str)

    motor1.put(next_pos, wait=True)
    
#    print("In position. Triggering something else...")
#    sys.stdout.flush()

    status.put("Triggering something else ...")

    LED.put(1)
    time.sleep(5)
    LED.put(0)
    if (script_status.value == 2):
        break
    
if (script_status.value == 2):
    status.put("Scan canceled")
else:
    status.put("Done")

script_status.put(0)
#print("Done!")
