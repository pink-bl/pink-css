# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 09:43:01 2018

@author: julius
"""


import epics
import numpy
import time
import sys
from xraydb import XrayDB

xr      = XrayDB()
PVpath  = 'PINK:VM10:ATTEN'

OBJ_BeamEnergy        = epics.PV(PVpath + ':indivTrans_photonEnergy_ao.VAL')
OBJ_TotalTransmission = epics.PV(PVpath + ':indivTrans_calcTotalTrans_selectedFilters_ai.VAL')
OBJ_InfoMessage       = epics.PV(PVpath + ':indivTrans_status_calc_stringin')   # holds up tp 40 characters

energyPhoton        = OBJ_BeamEnergy.get()
totalTransmission   = 1
um_to_cm            = 1/10000       # conversion factor to converts thicknesses in um as in imported array to cm as needed for calculation
OBJ_InfoMessage.put('Calculation starts...')

#if ((energyPhoton < 250) or (energyPhoton > 200000)):
#    OBJ_InfoMessage.put('Photon energy MUST be >250eV and <200keV')
#    time.sleep(2)
#    OBJ_InfoMessage.put('abort calculation...')
#    time.sleep(2)
#    OBJ_InfoMessage.put('Change photon energy !!!')
#    sys.exit()

def CalcTransmission(crossSection, density, thickness):
    # thickness in um !!, density in g/cm³, crossSection in cm²/g
    try: 
        exponent = -1.0 * crossSection 
    except:
        OBJ_InfoMessage.put('Problem with cross section')
        time.sleep(2)
        sys.exit()
    
    try:
        exponent = exponent * density
    except:
        OBJ_InfoMessage.put('Problem with density')
        time.sleep(2)
        sys.exit()   

    try:
        exponent = exponent * um_to_cm * thickness
    except:
        OBJ_InfoMessage.put('Problem with density')
        time.sleep(2)
        sys.exit()    
    
    absorption = numpy.exp(exponent)
    return absorption


def GetCrossSection(element):
    try:
        crossSection = xr.cross_section_elam(element, energyPhoton, kind='photo')
        print(element + ' cross section: ' + str(crossSection) + '[cm^2/g]')
        return crossSection
    except:
        print('Problem to get cross section for entry:' + element)
        OBJ_InfoMessage.put('Problem: cross section for ' + element)
        time.sleep(2)
        sys.exit()
        
        
def GetELAMdensity(element):
    try:
        density = xr.density(element)
        epics.PV(PVpath + ':indivTrans_density_filter' + str(filterNum) + '_ai').put(density)
        return density
    except:
        print('Problem to get ELAM density for:' + element)
        OBJ_InfoMessage.put('Problem to get density for:' + element) 
        time.sleep(2)
        sys.exit()
        
def GetUserDensity():
    try: 
        density = epics.PV(PVpath + ':indivTrans_indivDensity_filter' + str(filterNum) + '_ao').get()  
#        print(density)
        epics.PV(PVpath + ':indivTrans_density_filter' + str(filterNum) + '_ai').put(density)
        return density
    except:
        print('Problem to get user density for:' + element)
        OBJ_InfoMessage.put('Problem to get user density') 
        time.sleep(2)
        sys.exit()


for filterNum in range(1,5):
#    print(filterNum)
    active           = epics.PV(PVpath + ':indivTrans_activate_filter' + str(filterNum)).get()
#    print(active)
    if active:
#        print('step# 3b')
        element      = epics.PV(PVpath + ':indivTrans_material_filter' + str(filterNum)).get()
        thickness_um = epics.PV(PVpath + ':indivTrans_thickness_filter' + str(filterNum) + '_ao').get()
        print(element + ' thickness: ' + str(thickness_um) + ' um')
        crossSection = GetCrossSection(element)
        userDensity  = epics.PV(PVpath + ':indivTrans_activate_indivDensity_filter' + str(filterNum)).get()
        if userDensity:
#            print('step# 4')
            density  = GetUserDensity()     
        else:
            density = GetELAMdensity(element)
        filterTransmission  = CalcTransmission(crossSection, density, thickness_um)
        OBJ_InfoMessage.put('trans is ' + str(filterTransmission))
        time.sleep(0.5)
        epics.PV(PVpath + ':indivTrans_filter' + str(filterNum) + '_ai').put(filterTransmission)
        totalTransmission   = totalTransmission * filterTransmission

totalTransInPerCent = totalTransmission * 100
OBJ_TotalTransmission.put(totalTransInPerCent)
OBJ_InfoMessage.put('Calculation finished!')






















