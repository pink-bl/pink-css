# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 13:50:10 2018

@author: julius
"""

import epics
import numpy
import time
import sys

from beam_attenuation_filter_thicknesses import MaterialDensity
from beam_attenuation_filter_thicknesses import MaterialThicknessesAttenuatorA 
from beam_attenuation_filter_thicknesses import MaterialThicknessesAttenuatorB
from beam_attenuation_filter_thicknesses import MaterialThicknessesAttenuatorC
from beam_attenuation_filter_thicknesses import AbsorptionCrossSection_C 
from beam_attenuation_filter_thicknesses import AbsorptionCrossSection_AL
from beam_attenuation_filter_thicknesses import AbsorptionCrossSection_TA
from beam_attenuation_filter_thicknesses import FieldsMbbo_str

PVpath = 'PINK:VM10:ATTEN'

OBJ_givenBeamEnergy  = epics.PV(PVpath + ':photonEnergy_ao.VAL')
OBJ_selectedFilterA  = epics.PV(PVpath + ':filterA_mbbo.RVAL')
OBJ_selectedFilterB  = epics.PV(PVpath + ':filterB_mbbo.RVAL')
OBJ_selectedFilterC  = epics.PV(PVpath + ':filterC_mbbo.RVAL')
OBJ_calcTransmission = epics.PV(PVpath + ':calcTotalTrans_selectedFilters_ai.VAL')
OBJ_infoMessageCalc  = epics.PV(PVpath + ':Status_calc_stringin')   # holds up tp 40 characters

filterA = int(OBJ_selectedFilterA.get())
filterB = int(OBJ_selectedFilterB.get())
filterC = int(OBJ_selectedFilterC.get())
energyPhoton = OBJ_givenBeamEnergy.get()

density_C = MaterialDensity[0]
density_AL = MaterialDensity[1]
density_TA = MaterialDensity[2]

um_to_cm = 1/10000       # conversion factor to converts thicknesses in um as in imported array to cm as needed for calculation
update_filterNames = 1   # copies filternames from FieldsMbbo_str to the three MBBO PVs and all filter AI

def CalcCrossSection(energyPhoton, crossSectionLIST):
    # choose calculation method: 1 = linear regression   
    Eph = energyPhoton 
    absorptionCrossSection = -1.0
    method = 1     # linear regression
    if (method == 1):
        for i in range(0,len(crossSectionLIST)):            # number of energy sections between edges
            for j in range(0,len(crossSectionLIST[i])-1):   # number of energy values in one section i
                E1  = crossSectionLIST[i][j][0]
                E2  = crossSectionLIST[i][j+1][0]
                CS1 = crossSectionLIST[i][j][1]
                CS2 = crossSectionLIST[i][j+1][1]
                if ((Eph >= E1) and (Eph < E2)):            # looks always at energies above edge energy of energy section
                    absorptionCrossSection = CS1 + (Eph - E1)*(CS2-CS1)/(E2-E1)
    return absorptionCrossSection
    
def CalcTransmission(crossSection, density, thickness):
    # thickness in um !!, density in g/cm³, crossSection in cm²/g
    exponent = -1.0 * crossSection * density * um_to_cm * thickness 
    absorption = numpy.exp(exponent)
    return absorption

if ((energyPhoton < 2000) or (energyPhoton > 10000)):
    OBJ_infoMessageCalc.put('Photon energy MUST be >2keV and <10keV')
    time.sleep(2)
    OBJ_infoMessageCalc.put('abort calculation...')
    time.sleep(2)
    OBJ_infoMessageCalc.put('Change photon energy !!!')
    sys.exit()
    
OBJ_infoMessageCalc.put('calculating...')
time.sleep(0.5)

crossSec_C  = CalcCrossSection(energyPhoton, AbsorptionCrossSection_C)
crossSec_AL = CalcCrossSection(energyPhoton, AbsorptionCrossSection_AL)
crossSec_TA = CalcCrossSection(energyPhoton, AbsorptionCrossSection_TA) 

#Calculate transmission for all filters individually:
numFiltersInA = len(MaterialThicknessesAttenuatorA)
numFiltersInB = len(MaterialThicknessesAttenuatorB)
numFiltersInC = len(MaterialThicknessesAttenuatorC)

for x in range(0,numFiltersInA):
    d_array = MaterialThicknessesAttenuatorA
    filterHolder = 'A'
    OBJ_infoMessageCalc.put('calculating filter' + filterHolder + '0' + str(x))
    if (x < 10):
        ai_name         = PVpath + ':trans_filter' + filterHolder + '0' + str(x) + '_ai'
        ai_description  = ai_name + '.DESC'
    else:
        ai_name         = PVpath + ':trans_filter' + filterHolder + str(x) + '_ai'
        ai_description  = ai_name + '.DESC'        
    transmission_filter = CalcTransmission(crossSec_C, density_C, d_array[x][0]) * CalcTransmission(crossSec_AL, density_AL, d_array[x][1]) * CalcTransmission(crossSec_TA, density_TA, d_array[x][2])
    epics.PV(ai_name).put(transmission_filter)
    if update_filterNames:
        mbbo_name       = PVpath + ':filter' + filterHolder + '_mbbo.' + FieldsMbbo_str[x]
        epics.PV(ai_description).put(d_array[x][3])
        epics.PV(mbbo_name).put(d_array[x][3])
        

for x in range(0,numFiltersInB):
    d_array = MaterialThicknessesAttenuatorB
    filterHolder = 'B'
    OBJ_infoMessageCalc.put('calculating filter' + filterHolder + '0' + str(x))
    if (x < 10):
        ai_name         = PVpath + ':trans_filter' + filterHolder + '0' + str(x) + '_ai'
        ai_description  = ai_name + '.DESC'
    else:
        ai_name         = PVpath + ':trans_filter' + filterHolder + str(x) + '_ai'
        ai_description  = ai_name + '.DESC'        
    transmission_filter = CalcTransmission(crossSec_C, density_C, d_array[x][0]) * CalcTransmission(crossSec_AL, density_AL, d_array[x][1]) * CalcTransmission(crossSec_TA, density_TA, d_array[x][2])
    epics.PV(ai_name).put(transmission_filter)
    if update_filterNames:
        mbbo_name       = PVpath + ':filter' + filterHolder + '_mbbo.' + FieldsMbbo_str[x]
        epics.PV(ai_description).put(d_array[x][3])
        epics.PV(mbbo_name).put(d_array[x][3])


for x in range(0,numFiltersInC):
    d_array = MaterialThicknessesAttenuatorC
    filterHolder = 'C'
    OBJ_infoMessageCalc.put('calculating filter' + filterHolder + '0' + str(x))
    if (x < 10):
        ai_name         = PVpath + ':trans_filter' + filterHolder + '0' + str(x) + '_ai'
        ai_description  = ai_name + '.DESC'
    else:
        ai_name         = PVpath + ':trans_filter' + filterHolder + str(x) + '_ai'
        ai_description  = ai_name + '.DESC'        
    transmission_filter = CalcTransmission(crossSec_C, density_C, d_array[x][0]) * CalcTransmission(crossSec_AL, density_AL, d_array[x][1]) * CalcTransmission(crossSec_TA, density_TA, d_array[x][2])
    epics.PV(ai_name).put(transmission_filter)
    if update_filterNames:
        mbbo_name       = PVpath + ':filter' + filterHolder + '_mbbo.' + FieldsMbbo_str[x]
        epics.PV(ai_description).put(d_array[x][3])
        epics.PV(mbbo_name).put(d_array[x][3])
        

# Calculate thickness of each element and convert from um to cm:
totalThickness_C    = (MaterialThicknessesAttenuatorA[filterA][0] + MaterialThicknessesAttenuatorB[filterB][0] + MaterialThicknessesAttenuatorC[filterC][0])
totalThickness_AL   = (MaterialThicknessesAttenuatorA[filterA][1] + MaterialThicknessesAttenuatorB[filterB][1] + MaterialThicknessesAttenuatorC[filterC][1])
totalThickness_TA   = (MaterialThicknessesAttenuatorA[filterA][2] + MaterialThicknessesAttenuatorB[filterB][2] + MaterialThicknessesAttenuatorC[filterC][2])

# Calculate total transmission for selected filters:
transmission_C      = CalcTransmission(crossSec_C,    density_C,  totalThickness_C)
transmission_AL     = CalcTransmission(crossSec_AL,   density_AL, totalThickness_AL)
transmission_TA     = CalcTransmission(crossSec_TA,   density_TA, totalThickness_TA)

totalTransmission = transmission_C * transmission_AL * transmission_TA
print(totalTransmission)
transmission_perCent = (totalTransmission)*100

OBJ_calcTransmission.put(transmission_perCent)
OBJ_infoMessageCalc.put('Finished!')




