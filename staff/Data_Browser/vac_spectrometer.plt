<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<databrowser>
    <title></title>
    <save_changes>true</save_changes>
    <show_legend>false</show_legend>
    <show_toolbar>true</show_toolbar>
    <grid>false</grid>
    <scroll>true</scroll>
    <update_period>10.0</update_period>
    <scroll_step>20</scroll_step>
    <start>-30 min</start>
    <end>now</end>
    <archive_rescale>NONE</archive_rescale>
    <background>
        <red>255</red>
        <green>255</green>
        <blue>255</blue>
    </background>
    <title_font>Noto Sans|13|1</title_font>
    <label_font>Noto Sans|9|1</label_font>
    <scale_font>Noto Sans|8|0</scale_font>
    <legend_font>Noto Sans|8|0</legend_font>
    <axes>
        <axis>
            <visible>true</visible>
            <name>Pressure</name>
            <use_axis_name>true</use_axis_name>
            <use_trace_names>true</use_trace_names>
            <right>false</right>
            <color>
                <red>0</red>
                <green>0</green>
                <blue>0</blue>
            </color>
            <min>5.585988020572557</min>
            <max>6.6282670860827295</max>
            <grid>true</grid>
            <autoscale>true</autoscale>
            <log_scale>true</log_scale>
        </axis>
    </axes>
    <annotations>
    </annotations>
    <pvlist>
        <pv>
            <display_name>FV</display_name>
            <visible>true</visible>
            <name>PINK:MAXA:S2Measure</name>
            <axis>0</axis>
            <color>
                <red>78</red>
                <green>166</green>
                <blue>253</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
        </pv>
        <pv>
            <display_name>HV</display_name>
            <visible>true</visible>
            <name>PINK:MAXA:S4Measure</name>
            <axis>0</axis>
            <color>
                <red>254</red>
                <green>88</green>
                <blue>88</blue>
            </color>
            <trace_type>AREA</trace_type>
            <linewidth>2</linewidth>
            <point_type>NONE</point_type>
            <point_size>2</point_size>
            <waveform_index>0</waveform_index>
            <period>0.0</period>
            <ring_size>5000</ring_size>
            <request>OPTIMIZED</request>
        </pv>
    </pvlist>
</databrowser>